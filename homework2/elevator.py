## \addtogroup Hw0x02 Homework 0x02
#  Elevator finite state machine
#  @{

""" @file elevator.py
    @brief A finite state machine representing a 2 storey elevator
    @author Tyler Olson
    @date January 25, 2021
"""

from random import randint
from time import sleep


class State:
    """
    @brief Holds the current state of the elevator (as given in Homework0x00)
    """

    def __init__(self, motor=2, button_1=0, button_2=0):
        """
        @param motor Sets the motor to still(0), moving up(1), or moving down(1)
        @param button_1 State of the button for floor 1 (0=OFF, 1=ON)
        @param button_2 State of the button for floor 1 (0=OFF, 1=ON)
        """
        self.motor = motor
        self.button_1 = button_1
        self.button_2 = button_2

        # Assume not at either floor
        self.first = 0
        self.second = 0


# Global animation sequence for the elevator animation
"""@brief A sequence of frames for a text-based animation of an elevator floor dial"""
frame_sequence = ["|   ___.      |",
                  "|   -__.      |",
                  "|   `-..      |",
                  "|   `--.      |",
                  "|    `-.      |",
                  "|     \\.      |",
                  "|      !      |",
                  "|      ./     |",
                  "|      .-`    |",
                  "|      .--`   |",
                  "|      ..-'   |",
                  "|      .__-   |",
                  "|      .___   |"
                  ]


def main():
    """
    @brief Simulates a finite state machine representing a 2-storey elevator
    @details Indefinitely simulates a 2-storey elevator moving based on random button presses. All state changes related
             to buttons are handed within this function, however other changes to state are handled in helper functions
             in order to enhance OOP encapsulation.
    """

    # Create the elevator state and set it to move down
    state = State()
    move_down(state)

    # Loop indefinitely
    while True:
        if state.first == 1:
            if state.button_1 == 1:
                state.button_1 = ignore_button('1')
            if state.button_2 == 1:
                move_up(state)

                # Elevator reached floor 2
                state.button_2 = 0
                print("You have arrived at floor 2")

        elif state.second == 1:
            if state.button_2 == 2:
                state.button_2 = ignore_button('2')
            if state.button_1 == 1:
                move_down(state)

                # Elevator reached floor 1
                state.button_1 = 0
                print("You have arrived at floor 1")

        state.motor = 0

        # randomly press buttons
        press_at_random(state)
        sleep(2)


def press_at_random(state):
    """
    @brief Randomly presses button_1 and button_2
    @param state The current state of the elevator
    """

    if randint(0, 1) == 1:
        state.button_1 = 1
        print("Pressed button 1.")
    if randint(0, 1) == 1:
        state.button_2 = 1
        print("Pressed button 2.")


def ignore_button(btn_name):
    """
    @brief Reports that the given button was ignored.
    @param btn_name A string to name the button
    @return Always returns 0
    """
    print("Ignored button " + btn_name + ".")
    return 0


def move_up(state):
    """
    @brief Moves the elevator up to the second floor.
    @param state The current state of the elevator
    """

    state.first = 0
    state.motor = 1

    # Show elevator moving animation
    print("Elevator is moving up")
    sleep(1)
    animate_console(frame_sequence)
    state.second = 1


def move_down(state):
    """
    @brief Moves the elevator down to the first floor.
    @param state The current state of the elevator
    """

    state.second = 0
    state.motor = 2

    # Show elevator moving animation
    print("Elevator is moving down")
    sleep(1)
    animate_console(reversed(frame_sequence))
    state.first = 1


def animate_console(frames):
    for frame in frames:
        print("\r", frame, end="")
        sleep(0.2)
    print("")


if __name__ == '__main__':
    main()

## @}
#
