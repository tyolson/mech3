## \addtogroup Lab0x01 Lab0x01
#  Fibonacci lab
#  @{

""" @file fibonacci.py
    @brief The fibonacci file
    @author Tyler Olson
    @date January 21, 2021
"""

import numpy as np


def main():
    """
    @brief This functions as the user I/O and will prompt the user for the Fibonacci number they wish to calculate.
    """
    partials = {}

    while True:
        try:
            idx = int(input('Desired Fibonacci number (must be a positive integer): '))
        except ValueError:
            idx = -1
        except KeyboardInterrupt:
            print()
            return

        if idx >= 0:
            try:
                res = fib(idx, partials)
            except OverflowError:
                res = fib2(idx, partials)

            print('Fibonacci number at index {:} is {:d}.'.format(idx, res))
        else:
            print('ERROR: Malformed input')


def fib(idx, partials):
    """
    @brief Calculates the Fibonacci number at an index.
    @details This function calculates a Fibonacci number at a specific index using a truncated form of Binet's formula.
      Partial solutions are stored in partials.
    @param idx An integer specifying the index of the desired Fibonacci number
    @param partials A dictionary containing previously calculated solutions
    @return Returns the Fibonacci number with index idx
    """

    if idx in partials:
        return partials[idx]

    if (idx - 1) in partials and (idx - 2) in partials:
        res = partials[idx - 1] + partials[idx - 2]
    else:
        res = int(np.floor(((((1 + np.sqrt(5)) / 2) ** idx) / np.sqrt(5)) + 1 / 2))

    return _populate_partials(idx, res, partials)


def fib2(idx, partials):
    """
    @brief Calculates the Fibonacci number at an index.
    @details This function calculates a Fibonacci number at a specific index using an iterative method.
      Partial solutions are stored in partials.
    @param idx An integer specifying the index of the desired Fibonacci number
    @param partials A dictionary containing previously calculated solutions
    @return Returns the Fibonacci number with index idx
    """

    first = 0
    second = 1

    for i in range(2, idx):
        res = _populate_partials(i, first + second, partials)
        first = second
        second = res

    # noinspection PyUnboundLocalVariable
    return res


def _populate_partials(idx, val, partials):
    """
    @brief This function stores the solution (key, value) pair in partials.
    @param idx An integer specifying the index of the Fibonacci number val
    @param val The Fibonacci number at index idx
    @param partials A dictionary containing previously calculated solutions
    @return Returns the value val
    """
    partials[idx] = val
    return val


if __name__ == '__main__':
    main()

## @}
#
