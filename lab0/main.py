## \addtogroup Lab0x00
#  @{
#  @file main_lab2.py
#  Brief doc for main_lab2.py
#
#  Detailed doc for main_lab2.py
#
#  @author Your Name
#
#  @copyright License Info
#
#  @date January 1, 1970
#
#

import motor

## A motor driver object
moto = motor.MotorDriver()

moto.set_duty_cycle(50)

## @}
#
