#!/bin/bash

# nucleo_connect
screen -S nucleo /dev/ttyACM0 115200

# nucleo_disconnect
screen -X -S nucleo quit
