## \addtogroup Lab0x03 Lab0x03
#  @{

""" @file Button.py
    @brief Button class for detecting types of pushes
    @details In this lab, the user can play a rudimentary version of Simon Says with the Nucleo Board.
             This button will report a sequence of dots and dashes to a callback function.
    @author Tyler Olson
    @date February 17, 2021
"""

import utime
import micropython
from pyb import Pin, Timer, ExtInt
from main import LD2, _DEBUG

## @brief Threshold for differentiating dots vs dashes in ms
#
DASH_THRESHOLD_MS = 250
BIT_MASK = 0x00FFFFFF


class Button(Pin):
    """
    @brief A Button class that reports dots and dashes.
    @details This button class extends the pyb.Pin class, making use of a pyb.Timer to limit reading how the button is
             pressed.  Buttons can read dots (short press < 250ms) and dashes (long press =< 250ms).  The button
             will wait a maximum of 5s for new presses.
    """

    def start_disable_timer(self, timer: Timer = None):
        """
        @brief Resets or starts the timeout countdown.  If the timeout is reached, the button will be disabled.
        @param timer The countdown timer
        """
        if timer is None:
            timer = self.timer

        timer.init(freq=0.2, callback=self.disable)
        if _DEBUG:
            print("[", utime.ticks_diff(utime.ticks_ms(), self.epoch), "]  Disable timer started")

    def disable(self, trigger: Timer):
        """
        @brief Disables the button and runs the callback function specified upon Button creation.
        @param trigger The timer that disabled the button
        """
        trigger.deinit()
        self.interrupt.disable()
        if _DEBUG:
            print("Timeout: User took too long")

        # launch the callback function for main to handle (if it exists)
        if self.callback is not None:
            micropython.schedule(self.callback, self.sequence)

    def enable(self):
        """
        @brief Enables the button after having been disabled
        """

        self.interrupt.enable()
        self.triggered = Pin.IRQ_FALLING
        self.sequence = 0

        if _DEBUG:
            print("[", utime.ticks_diff(utime.ticks_ms(), self.epoch), "] Button Enabled ( pin", self.pin(), ")")

    def trigger(self, irq_src: int):
        """
        @brief Collects and reports a button action and starts a timeout timer.
        @param irq_src The interrupt request source line number (required as an interrupt callback).
        """

        # reset the timeout interrupt
        self.timer.deinit()
        # schedule a timer to disable the button upon timeout
        micropython.schedule(self._start_disable_timer, self.timer)

        # update the current status and perform appropriate action
        if self.triggered == Pin.IRQ_FALLING:
            self.press(irq_src)
        else:
            self.release(irq_src)

    def press(self, irq_src: int = None):
        """
        @brief Recognizes the button press has been started.
        @param irq_src The interrupt request source line number (required as an interrupt callback).
        """

        if _DEBUG:
            print("[", utime.ticks_diff(utime.ticks_ms(), self.epoch), "] Button Pressed ( pin", irq_src, ")")

        LD2.high()
        self.triggered = Pin.IRQ_RISING
        self.triggered_time = utime.ticks_ms()

    def release(self, irq_src: int = None):
        """
        @brief Recognizes the button press has finished.  Updates the sequence with the type of press.
        @param irq_src The interrupt request source line number (required as an interrupt callback).
        """

        if _DEBUG:
            print("[", utime.ticks_diff(utime.ticks_ms(), self.epoch), "] Button Released ( pin", irq_src, ")")

        LD2.low()
        self.triggered = Pin.IRQ_FALLING
        triggered_time = utime.ticks_diff(utime.ticks_ms(), self.triggered_time)
        # increment length:
        self.sequence = ((self.sequence + (1 << 24)) & ~BIT_MASK) | ((self.sequence * 2) & BIT_MASK)

        # add next bit (0 if dot and 1 if dash)
        if triggered_time >= DASH_THRESHOLD_MS:
            self.sequence += 1

        if _DEBUG:
            print("[", utime.ticks_diff(utime.ticks_ms(), self.epoch), "] detected a", "-" if triggered_time >= DASH_THRESHOLD_MS else ".")
            print("         sequence: ", self.sequence & BIT_MASK)
            print("         length: ", self.sequence >> 24)

    def __init__(self, callback=None):
        """
        @brief Create a Button object.
        @param callback A function to optionally be called once the button is disabled
        """

        super().__init__(Pin.cpu.C13, Pin.IN, Pin.PULL_NONE)
        if _DEBUG:
            self.epoch = utime.ticks_ms()

        # Interrupt for button detection and timeout handling
        self.interrupt = ExtInt(self.name(), mode=ExtInt.IRQ_RISING_FALLING, pull=self.pull(), callback=self.trigger)
        self.timer = Timer(2)
        self._start_disable_timer = self.start_disable_timer

        # default state is that the button has not been pressed (i.e. previous edge was FALLING)
        self.triggered = Pin.IRQ_FALLING
        self.triggered_time = None

        # function to return to when the button is disabled
        self.callback = callback

        ## @brief sequence of dots and dashes that are recorded
        #  @details A bitfield whose first 24 bits are slots for dots (0) or dashes (1), and the last 8 bits encode the
        #           length
        self.sequence = 0

## @}
#
